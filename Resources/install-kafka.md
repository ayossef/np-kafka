# Install Kafka
Kafka (like other lots of Apache project) it is based on Java. 

## Installation Steps:
0. Install Required Tools
```bash
apt install wget nano
```
1. Install Java
```bash
apt install default-jdk
```
2. Download and untar Kafka 
```bash
# download kafka
wget https://downloads.apache.org/kafka/3.4.0/kafka_2.13-3.4.0.tgz
# extract
tar zxf kafka_2.13-3.4.0.tgz
# move to /opt
mv kafka_2.13-3.4.0 /opt/kafka
```
3. Configure Kafka and Zookeeper
```bash
nano /etc/systemd/system/kafka.service
nano /etc/systemd/system/zookeeper.service

systemctl daemon-reload
systemctl enable zookeeper 
systemctl enable kafka 
systemctl start zookeeper
systemctl start kafka
```
4. Run and verify Kafka