# Install Kafka Cluster on Docker


Installaiton Steps

1. Install Docker
2. Create a container
3. Install kafka and Zookeeper inside this container

## First Install Docker
In order to intall docker we have two options:
1. Install directly using APT via ubuntu servers
2. Install using APT via docker servers


we are using the second option, why?
because, the most recent versions are avaialble on docker servers first.
1. Update the local repo
```bash
sudo apt update
```

2. Install some package for verifiction
```bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common
```

3. Add the keys of docker servers
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
#verify keys adding
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

4. Install docker using apt via docker servers
```bash
sudo apt update
sudo apt install docker-ce
```

5. Add current user to docker group
```bash
sudo usermod -aG docker nobleprog
```


## Second create an ubuntu container
We need first to pull an Ubuntu Image then we run this image

1.  Pulling ubuntu image
```bash
docker pull ubuntu
```

2. Run a container
```bash
docker run ubuntu
# let us give a name for the container (--name)
docker run --name kafka ubuntu
# let us give also a port mapping (-p)
docker run --name kafka -p9090:9092 ubuntu 
```